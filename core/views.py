from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.views.generic import TemplateView, FormView

from core.forms import SignUpForm, SignInForm


class SignUpView(FormView):
    form_class = SignUpForm
    template_name = 'core/sign_up.html'
    
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('home'))
        return super(SignUpView, self).get(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, message="Your account has been successfully created",
                             extra_tags="alert alert-success")
            return HttpResponseRedirect(reverse_lazy('sign-in'))
        return render(request, self.template_name, context={'form': form})


class SignInView(FormView):
    form_class = SignInForm
    template_name = 'core/sign_in.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('home'))
        return super(SignInView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            username_or_email = form.cleaned_data.get('username_or_email')
            password = form.cleaned_data.get('password')

            pretended_user = User.objects.filter(
                Q(username__exact=username_or_email) |
                Q(email__iexact=username_or_email)
            ).first()
            if pretended_user is None:
                messages.error(request, message="This email or username is not associated with any account",
                               extra_tags="alert alert-danger")
                return render(request, self.template_name, context={'form': form})

            username = pretended_user.username
            user = authenticate(request, username=username, password=password)

            if user is None:
                messages.error(request, message="Username or password is incorrect", extra_tags="alert alert-danger")
                return render(request, self.template_name, context={'form': form})

            login(request, user)
            return HttpResponseRedirect(reverse_lazy('home'))

        return render(request, self.template_name, context={'form': form})


class SignOutView(LoginRequiredMixin, TemplateView):
    template_name = 'core/sign_out.html'

    login_url = reverse_lazy('sign-in')
    redirect_field_name = 'next'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(SignOutView, self).get(request, *args, **kwargs)


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'core/index.html'

    login_url = reverse_lazy('sign-in')
    redirect_field_name = 'next'  # can be import from django.contrib.auth.mixins import REDIRECT_FIELD_NAME
